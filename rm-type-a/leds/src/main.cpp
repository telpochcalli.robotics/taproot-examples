#include "aruwlib/drivers_singleton.hpp"
#include "aruwlib/rm-dev-board-a/board.hpp"

using aruwlib::Drivers;

static constexpr uint32_t LED_BLINK_PERIOD = 1000;

int main()
{
    /*
     * NOTE: We are using DoNotUse_getDrivers here because in the main
     *      robot loop we must access the singleton drivers to update
     *      IO states and run the scheduler.
     */
    aruwlib::Drivers *drivers = aruwlib::DoNotUse_getDrivers();

    Board::initialize();
    drivers->leds.init();

    bool aSet = true;

    while (1)
    {
        modm::delay_ms(LED_BLINK_PERIOD / 2);
        drivers->leds.set(aruwlib::gpio::Leds::A, !aSet);
        aSet = !aSet;
    }
    return 0;
}
