#include "aruwlib/drivers_singleton.hpp"
#include "aruwlib/rm-dev-board-a/board.hpp"

using aruwlib::Drivers;

static constexpr uint32_t SERVO_UPDATE_TIME = 100;
static constexpr float MIN_SERVO_PWM = 0.1f;
static constexpr float MAX_SERVO_PWM = 0.2f;

int main()
{
    /*
     * NOTE: We are using DoNotUse_getDrivers here because in the main
     *      robot loop we must access the singleton drivers to update
     *      IO states and run the scheduler.
     */
    aruwlib::Drivers *drivers = aruwlib::DoNotUse_getDrivers();

    Board::initialize();
    drivers->pwm.init();
    drivers->digital.init();

    while (1)
    {
        drivers->pwm.write(
            drivers->digital.read(aruwlib::gpio::Digital::InputPin::A) ? MAX_SERVO_PWM
                                                                       : MIN_SERVO_PWM,
            aruwlib::gpio::Pwm::X);

        modm::delay_ms(SERVO_UPDATE_TIME);
    }
    return 0;
}
