#include "aruwlib/drivers_singleton.hpp"
#include "aruwlib/rm-dev-board-a/board.hpp"
#include "aruwlib/algorithms/smooth_pid.hpp"

using aruwlib::Drivers;

static constexpr aruwlib::motor::MotorId MOTOR_ID = aruwlib::motor::MOTOR1;
static constexpr aruwlib::can::CanBus CAN_BUS = aruwlib::can::CanBus::CAN_BUS1;
static constexpr int DESIRED_RPM = 3000;

static aruwlib::arch::PeriodicMilliTimer sendMotorTimeout(2);
static aruwlib::algorithms::SmoothPid pidController(20, 0, 0, 0, 8000, 1, 0, 1, 0);

int main()
{
    /*
     * NOTE: We are using DoNotUse_getDrivers here because in the main
     *      robot loop we must access the singleton drivers to update
     *      IO states and run the scheduler.
     */
    aruwlib::Drivers *drivers = aruwlib::DoNotUse_getDrivers();

    Board::initialize();

    aruwlib::motor::DjiMotor motor(drivers, MOTOR_ID, CAN_BUS, false, "cool motor");

    motor.initialize();

    while (1)
    {
        if (sendMotorTimeout.execute())
        {
            pidController.runControllerDerivateError(DESIRED_RPM - motor.getShaftRPM(), 1);
            motor.setDesiredOutput(static_cast<int32_t>(pidController.getOutput()));
            drivers->djiMotorTxHandler.processCanSendData();
        }

        drivers->canRxHandler.pollCanData();
        modm::delay_us(10);
    }
    return 0;
}
