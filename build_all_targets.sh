#!/bin/bash

for dir in ./rm-type-a/*; do
    if [[ -d "$dir" ]] && [[ "$dir" != "./aruwlib" ]]; then
        cd "$dir"
        lbuild build
        if [[ "$?" != 0 ]]; then
            echo "lbuild failed"
            exit 1
        fi
        scons build
        if [[ "$?" != 0 ]]; then
            echo "scons build failed"
            exit 1
        fi
        cd -
    fi
done

exit 0
