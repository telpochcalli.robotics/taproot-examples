#include "aruwlib/display/sh1106.hpp"
#include "aruwlib/drivers_singleton.hpp"
#include "aruwlib/rm-dev-board-a/board.hpp"

using aruwlib::Drivers;

static constexpr uint32_t LED_BLINK_PERIOD = 1000;

aruwlib::display::Sh1106<
#ifndef PLATFORM_HOSTED
    Board::DisplaySpiMaster,
    Board::DisplayCommand,
    Board::DisplayReset,
#endif
    128,
    64,
    false>
    display;

int main()
{
    /*
     * NOTE: We are using DoNotUse_getDrivers here because in the main
     *      robot loop we must access the singleton drivers to update
     *      IO states and run the scheduler.
     */
    aruwlib::Drivers *drivers = aruwlib::DoNotUse_getDrivers();

    Board::initialize();
    display.initializeBlocking();

    display << "Hello world";
    display.update();

    while (1)
    {
        display.updateNonblocking();
        modm::delay_us(10);
    }
    return 0;
}
