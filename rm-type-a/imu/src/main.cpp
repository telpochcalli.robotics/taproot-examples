#include "aruwlib/architecture/periodic_timer.hpp"
#include "aruwlib/drivers_singleton.hpp"
#include "aruwlib/rm-dev-board-a/board.hpp"

using aruwlib::Drivers;

aruwlib::arch::PeriodicMilliTimer updateImuTimeout(2);

float yaw;

int main()
{
    /*
     * NOTE: We are using DoNotUse_getDrivers here because in the main
     *      robot loop we must access the singleton drivers to update
     *      IO states and run the scheduler.
     */
    aruwlib::Drivers *drivers = aruwlib::DoNotUse_getDrivers();

    Board::initialize();
    drivers->mpu6500.init();

    while (1)
    {
        drivers->mpu6500.read();

        if (updateImuTimeout.execute())
        {
            drivers->mpu6500.calcIMUAngles();
            yaw = drivers->mpu6500.getYaw();
            // TODO use yaw
        }

        modm::delay_us(10);
    }
    return 0;
}
