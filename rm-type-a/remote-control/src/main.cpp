#include "aruwlib/drivers_singleton.hpp"
#include "aruwlib/rm-dev-board-a/board.hpp"

using aruwlib::Drivers;

int main()
{
    /*
     * NOTE: We are using DoNotUse_getDrivers here because in the main
     *      robot loop we must access the singleton drivers to update
     *      IO states and run the scheduler.
     */
    aruwlib::Drivers *drivers = aruwlib::DoNotUse_getDrivers();

    Board::initialize();
    drivers->remote.initialize();

    while (1)
    {
        drivers->remote.read();
        modm::delay_us(10);
    }
    return 0;
}
