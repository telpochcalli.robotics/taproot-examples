[![pipeline status](https://gitlab.com/aruw/controls/aruwlib-examples/badges/develop/pipeline.svg)](https://gitlab.com/aruw/controls/aruwlib-examples/-/commits/develop)
#ceja4nrvflirjntvñoa4infvraiñgtvnarwoñingtvroñinvrñionsoñinriwvgntrownitvrgvijn comit 
## aruwlib-examples

This repository contains small example projects to demonstrate various features in
[aruwlib](https://gitlab.com/aruw/controls/aruwlib). This repository currently has examples specific
to the RoboMaster Development Board Type A but examples for other development boards plan to be
added in the future.

## Examples

The following examples have been implemented:

- RoboMaster Development Board Type A:
    - [IMU](rm-type-a/imu/src/main.cpp) - The onboard Inertial Measurement Unit. Reads data such as
      orientation and acceleration from the internal mpu6500 IMU. Data can be retrieved via the
      `mpu6500` object.
    - [LEDs](rm-type-a/leds/src/main.cpp) - The example blinks one of the LEDs.
    - [OLED](rm-type-a/oled/src/main.cpp) - Prints "Hello World" if a [RoboMaster OLED
      display](https://store.dji.com/product/rm-development-board-oled) is connected to the
      development board.
    - [Remote Control](rm-type-a/remote-control/src/main.cpp) - Reads data from a connected DR16
      remote receiver. Use the API provided by the `remote` object to get remote data.
    - [Servo](rm-type-a/servo/src/main.cpp) - Changes the desired PWM value of a servo connected to
      the development board between some min and max values based on input from a digital pin.
    - [Velocity Motor Controller](rm-type-a/velocity-motor-controller/src/main.cpp) - Runs a PID
      controller to command a "DJI" motor (currently supported motors include any RoboMaster branded
      motor) connected on a particular CAN bus with a particular motor ID to some desired velocity.

## Using these examples

It is assumed you have a development environment set up that allows you to deploy to the development
board. See
[here](https://gitlab.com/aruw/controls/aruwlib/-/blob/develop/README.md#user-content-development-guide)
for setting up your development environment. Once this is done, you can use these examples via the
following steps:

1. Clone the repository. `cd` into the repository and update submodules (`git submodule update
   --init --recursive`).
2. `cd` into the example project that you would like to try out.
3. Run `lbuild build` to generate code necessary for the example.
4. Run `scons build` to build the example code.
5. Use either a J-Link or ST-Link to deploy the code to the development board you are using. If you
   are using an ST-Link, run `scons program` with the ST-Link plugged into your computer. If you are
   using a J-Link, use
   [Ozone](https://www.segger.com/products/development-tools/ozone-j-link-debugger/) to flash the
   built `.elf` file to the development board.
